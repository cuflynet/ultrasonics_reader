#include <stdio.h>
#include <unistd.h>     //Used for UART
#include <fcntl.h>      //Used for UART
#include <termios.h>    //Used for UART
#include <sys/ioctl.h>
#include <string>
#include <signal.h>
#include <time.h>
#include <string>
#include <vector>
#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>

#define IN_TO_M 0.0254

using namespace std;

unsigned int DIE = 0;

void exitHandler(int param)
{
    DIE = 1;
}

void findMinMax(const vector< float > ranges, float &min, float &max)
{
    min = 99999999;
    max = -99999999;
    for (int idx = 0; idx < ranges.size(); idx++) {
        if (ranges[idx] > max) {
            max = ranges[idx];
        }
        if (ranges[idx] < min) {
            min = ranges[idx];
        }
    }
}

void parseBuffer(string s, vector< float > &ranges)
{
    string delimiter = " ";
    size_t pos = 0;
    string token;

    int count = 0;
    while (((pos = s.find(delimiter)) != string::npos) && count < 4) { 
        token = s.substr(0, pos);
        ranges.push_back(stof(token)*IN_TO_M);
        s.erase(0, pos + delimiter.length());
        count++;
    }
}

void populateLaserScan(const vector< float > ranges, sensor_msgs::LaserScan &msg)
{  
    float range_min;
    float range_max;
    findMinMax(ranges, range_min, range_max);
    time_t cur_time = time(NULL);
    struct tm *ptm = gmtime(&cur_time); 
    
    msg.angle_min = -1;
    msg.angle_max = -1;
    msg.angle_increment = -1;
    msg.time_increment = -1;
    msg.scan_time = ptm->tm_hour*60*60 + ptm->tm_min*60 + ptm->tm_sec;
    msg.range_min = range_min;
    msg.range_max = range_max;
    msg.ranges.push_back(ranges[0]);
    msg.ranges.push_back(ranges[1]);
    msg.ranges.push_back(ranges[2]);
    msg.ranges.push_back(ranges[3]);
}

int main(int argc, char **argv)
{
    // SETUP SIGNAL HANDLER
    signal(SIGINT, exitHandler);
    signal(SIGABRT, exitHandler);
    signal(SIGILL, exitHandler);
    signal(SIGSEGV, exitHandler);
    signal(SIGTERM, exitHandler);

    // INITIALIZE ROS
    ros::init(argc, argv, "ultrasonics_reader_node");
    ros::NodeHandle nh;
    ros::Publisher usRangePub = nh.advertise<sensor_msgs::LaserScan>("ultrasonics_range", 1);

    //-------------------------
    //----- SETUP USART 0 -----
    //-------------------------
    //At bootup, pins 8 and 10 are already set to UART0_TXD, UART0_RXD (ie the alt0 function) respectively
    int uart0_filestream = -1;
    
    //OPEN THE UART
    //The flags (defined in fcntl.h):
    //  Access modes (use 1 of these):
    //      O_RDONLY - Open for reading only.
    //      O_RDWR - Open for reading and writing.
    //      O_WRONLY - Open for writing only.
    //
    //  O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests on the file can return immediately with a failure status
    //                                          if there is no input immediately available (instead of blocking). Likewise, write requests can also return
    //                                          immediately with a failure status if the output can't be written immediately.
    //
    //  O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the terminal device to become the controlling terminal for the process.
    uart0_filestream = open("/dev/ttyAMA0", O_RDONLY | O_NOCTTY | O_NDELAY);      //Open in non blocking read/write mode
    if (uart0_filestream == -1) {
        //ERROR - CAN'T OPEN SERIAL PORT
        printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
    }
    
    //CONFIGURE THE UART
    //The flags (defined in /usr/include/termios.h - see http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
    //  Baud rate:- B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800, B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
    //  CSIZE:- CS5, CS6, CS7, CS8
    //  CLOCAL - Ignore modem status lines
    //  CREAD - Enable receiver
    //  IGNPAR = Ignore characters with parity errors
    //  ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of line characters - don't use for bianry comms!)
    //  PARENB - Parity enable
    //  PARODD - Odd parity (else even)
    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;     //<Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);

    string line = "";
    bool firstLine = true;

    while (DIE == 0) {
        //----- CHECK FOR ANY RX BYTES -----
        if (uart0_filestream != -1) {
            int bytes_avail = 0;
            ioctl(uart0_filestream, FIONREAD, &bytes_avail);
            if (bytes_avail > 0) {
                // Read up to 255 characters from the port if they are there
                char rx_buffer[256] = { "\0" };
                int rx_length = read(uart0_filestream, (void*)rx_buffer, 1);      //Filestream, buffer to store in, number of bytes to read (max)
                if (rx_length > 0) {
                    //Bytes received
                    rx_buffer[rx_length] = '\0';
                    if ('\n' == rx_buffer[0]) {
                        if (!firstLine) {
                            vector< float > ranges;
                            parseBuffer(line, ranges);
                            sensor_msgs::LaserScan rangesMsg;
                            populateLaserScan(ranges, rangesMsg);
                            usRangePub.publish(rangesMsg);
                            ros::spinOnce();
                        }
                        line = "";
                        firstLine = false;
                    } else {
                        line = line + rx_buffer[0];
                    }
                }
            }
        }
    }

    //----- CLOSE THE UART -----
    printf("\nClosing UART\n");
    close(uart0_filestream);
}
