#define SERIAL Serial1
#define SERIAL_BAUD 9600
#define SERIAL_MODE SERIAL_8N1
#define NUM_SENSORS 4
const int ledPin = 13;
const int anPin1 = A0;
const int anPin2 = A1;
const int anPin3 = A2;
const int anPin4 = A3;
const int triggerPin1 = 19;
const int triggerPin2 = 20;
const int triggerPin3 = 21;
const int triggerPin4 = 22;
long distance1 = -1;
long distance2 = -1;
long distance3 = -1;
long distance4 = -1;

void setup() {
  Serial.begin(SERIAL_BAUD);  // sets the serial port to 9600
  SERIAL.begin(SERIAL_BAUD, SERIAL_MODE);
  pinMode(triggerPin1, OUTPUT);
  pinMode(triggerPin2, OUTPUT);
  pinMode(triggerPin3, OUTPUT);
  pinMode(triggerPin4, OUTPUT);
  pinMode(ledPin, OUTPUT);
}

void read_sensors(const int anPin, long &distance) {
  /*
  Scale factor is (Vcc/512) per inch. A 5V supply yields ~9.8mV/in
  Arduino analog pin goes from 0 to 1024, so the value has to be divided by 2 to get the actual inches
  */
  distance = analogRead(anPin)/2;
}

void trigger_sensor(const int triggerPin) {
  digitalWrite(ledPin, HIGH);
  digitalWrite(triggerPin, HIGH);
  delay(1);
  digitalWrite(triggerPin, LOW);
  digitalWrite(ledPin, LOW);
}

void print_all(){
  digitalWrite(ledPin, HIGH);

  Serial.print(distance1);
  Serial.print(" ");
  SERIAL.print(distance1);
  SERIAL.print(" ");
  
  Serial.print(distance2);
  Serial.print(" ");
  SERIAL.print(distance2);
  SERIAL.print(" ");

  Serial.print(distance3);
  Serial.print(" ");
  SERIAL.print(distance3);
  SERIAL.print(" ");

  Serial.print(distance4);
  Serial.print(" ");
  SERIAL.print(distance4);
  SERIAL.print(" ");

  Serial.println();
  SERIAL.println();
  digitalWrite(ledPin, LOW);
}

void loop() {
  trigger_sensor(triggerPin1);
  trigger_sensor(triggerPin2);
  trigger_sensor(triggerPin3);
  trigger_sensor(triggerPin4);

  delay(50);

  read_sensors(anPin1, distance1);
  read_sensors(anPin2, distance2);
  read_sensors(anPin3, distance3);  
  read_sensors(anPin4, distance4);
  
  print_all();
}
